var ua = navigator.userAgent.toLowerCase();
var isWx = true || ua.match(/MicroMessenger/i) == "micromessenger";
var vm = new Vue({
    el: '#jia-jian-fa',
    data: {
        isWx: isWx,
        section: 'config',//config, doing
        types: ['1'],
        count: 10,
        max: 20,
        right: 0,
        result: [],
        currentIdx: 0,
        tiku: [{
            id: 0,
            method: '+',
            var1: 0,
            var2: 0,
            result: 0,
            expect: 0
        }],
        errorStack: [],
        percentMark: 0,
        score: 0,
        showSubmit: false,
        resultIcon: 'weui-icon-success weui-icon_msg'
    },
    created: function () {
        if (isWx) {
            this.initShare();
        }
    },
    computed: {
        rightPercent: function () {
            console.log('rightPercent')
            console.log(this.percentMark);
            var right = _.filter(this.result, function (item) {
                return item === 1;
            });
            console.log(right)
            this.right = right.length;
            this.score = Number((right.length / this.count) * 100).toFixed(0);
            return this.score + '%';
        },
        errorPercent: function () {
            console.log('errorPercent')
            console.log(this.percentMark);
            var error = _.filter(this.result, function (item) {
                return item === 0;
            });
            console.log(error)
            console.log(this.max)
            return Number((error.length / this.count) * 100).toFixed(0) + '%';
        },
        current: function () {
            return this.tiku[this.currentIdx];
        },
        resultMsg: function () {
            this.percentMark;
            var msg = '';
            var icon = 'weui-icon-success weui-icon_msg';
            if (this.score == 100) {
                icon = 'weui-icon-success weui-icon_msg';
                msg = '宝宝太棒了，得了100分！'
            } else if (this.score > 95) {
                icon = 'weui-icon-success weui-icon_msg-primary';
                msg = '宝宝太遗憾了，差一点就得满分了'
            } else if (this.score > 80) {
                icon = 'weui-icon-success weui-icon_msg-primary';
                msg = '宝宝有些虚心，要继续努力哦'
            } else if (this.score > 70) {
                icon = 'weui-icon-warn weui-icon_msg-primary';
                msg = '宝宝一定要多练习哦'
            } else if (this.score > 60) {
                icon = 'weui-icon-warn weui-icon_msg-primary';
                msg = '宝宝刚及格，家长要努力哦'
            } else {
                icon = 'weui-icon-warn weui-icon_msg';
                msg = '为了孩子的未来，我们要努力哦'
            }
            this.resultIcon = icon;
            return msg;
        }
    },
    methods: {
        restart: function () {
            this.right = 0;
            this.result = [];
            this.currentIdx = 0;
            this.percentMark = 0;
            this.score = 0;
            this.section = 'doing';
        },
        reset: function () {
            window.location.reload()
        },
        startDoing: function () {
            this.section = 'doing';
            this.currentIdx = 0;
            this.tiku = [];
            var apiMap = {
                1: this.getPlusRandom,
                2: this.getMinusRandom
            };
            for (var i = 0; i < this.count; i++) {
                var rdm = _.random(0, this.types.length - 1);
                this.result.push(2);
                apiMap[this.types[rdm]].call(this, i);
            }
        },
        preQuestion: function () {
            this.showSubmit = false;
            if (this.currentIdx > 0) {
                this.currentIdx = this.currentIdx - 1;
            }
        },
        nextQuestion: function () {
            this.percentMark = this.percentMark + 1;
            this.result[this.currentIdx] = (this.current.result - this.current.expect === 0 ? 1 : 0);

            if (this.currentIdx == this.tiku.length - 1) {
                this.showSubmit = true;
            }
            if (this.currentIdx < this.tiku.length - 1) {
                this.currentIdx = this.currentIdx + 1;
            }
        },
        submit: function () {
            this.$emit('nextQuestion');
            this.section = 'result';
        },
        switchSection: function (section) {
            this.section = section;
        },
        showTiExpect: function (id) {
            for (var i = 0, len = this.tiku.length; i < len; i++) {
                var ti = this.tiku[i];
                if (ti.id == id) {
                    ti.show = true;
                }
            }

        },
        shareTo: function () {
            $.alert("点击右上角的  …  进行分析", "分享给其他小朋友");
        },
        initShare: function () {
            $.ajax({
                url: '/wx/jsapi/config',
                data: {
                    url: document.location.href
                },
                success: function (rs) {
                    if (rs.code == 1) {
                        wx.config({
                            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                            appId: rs.data.appId, // 必填，公众号的唯一标识
                            timestamp: rs.data.api.timestamp, // 必填，生成签名的时间戳
                            nonceStr: rs.data.api.noncestr, // 必填，生成签名的随机串
                            signature: rs.data.api.signature,// 必填，签名，见附录1
                            jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                        });
                        wx.ready(function () {
                            var title = "我家宝宝正在做题，带宝宝一起做题吧";
                            var link = window.location.href;
                            var imgUrl = 'http://www.lionsparty.com/static/img/favicon.png';
                            var desc = '专门让幼小做题的工具';
                            wx.onMenuShareTimeline({
                                title: title, // 分享标题
                                link: link, // 分享链接
                                imgUrl: imgUrl, // 分享图标
                                success: function () {
                                    // 用户确认分享后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消分享后执行的回调函数
                                }
                            });
                            wx.onMenuShareAppMessage({
                                title: title, // 分享标题
                                desc: desc, // 分享描述
                                link: link, // 分享链接
                                imgUrl: imgUrl, // 分享图标
                                type: '', // 分享类型,music、video或link，不填默认为link
                                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                                success: function () {
                                    // 用户确认分享后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消分享后执行的回调函数
                                }
                            });
                            wx.onMenuShareQQ({
                                title: title, // 分享标题
                                desc: desc, // 分享描述
                                link: link, // 分享链接
                                imgUrl: imgUrl, // 分享图标
                                success: function () {
                                    // 用户确认分享后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消分享后执行的回调函数
                                }
                            });
                            wx.onMenuShareWeibo({
                                title: title, // 分享标题
                                desc: desc, // 分享描述
                                link: link, // 分享链接
                                imgUrl: imgUrl, // 分享图标
                                success: function () {
                                    // 用户确认分享后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消分享后执行的回调函数
                                }
                            });
                            wx.onMenuShareQZone({
                                title: title, // 分享标题
                                desc: desc, // 分享描述
                                link: link, // 分享链接
                                imgUrl: imgUrl, // 分享图标
                                success: function () {
                                    // 用户确认分享后执行的回调函数
                                },
                                cancel: function () {
                                    // 用户取消分享后执行的回调函数
                                }
                            });
                        });
                    }
                }
            })
        },
        getPlusRandom: function (id) {
            var max = this.max;
            var v1 = _.random(1, max);
            var v2 = _.random(1, max - v1);
            if (v2 === 0) {
                v2 = _.random(max - v1);
            }
            this.tiku.push({
                id: id,
                show: false,
                method: '+',
                var1: v1,
                var2: v2,
                result: '',
                expect: v1 + v2
            });
        },
        getMinusRandom: function (id) {
            var max = this.max;
            var v1 = _.random(1, max);
            var v2 = _.random(1, max);
            if (v2 > v1) {
                this.tiku.push({
                    id: id,
                    show: false,
                    method: '-',
                    var1: v2,
                    var2: v1,
                    result: '',
                    expect: v2 - v1
                });
            } else {
                this.tiku.push({
                    id: id,
                    show: false,
                    method: '-',
                    var1: v1,
                    var2: v2,
                    result: '',
                    expect: v1 - v2
                });
            }
        }

    }
});
var share = {
    do: function (score) {

    }
}